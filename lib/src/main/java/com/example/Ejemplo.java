package com.example;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;

public class Ejemplo {

    public static void main(String[] args) {

        Integer[] numeros = {1,2,3,4,5,6,7,8,9,10,11,50,45,78};



        Observable  .fromArray(numeros)//TRAE LOS DATOS
                    .filter(numero -> (numero>4))//FILTRA LOS DATOS
                    .map(numero -> "["+numero+"]")//FORMATO A DATOS
                    .subscribe(numerosRecibidos -> System.out.println(numerosRecibidos) );

        /*CargarNumeros().map(numero -> String.valueOf("{" + numero + "}"))
                .subscribe(System.out::print);*/

    }

   /* private static Observable<Integer> CargarNumeros() {


        //CREANDO UN OBSERVABLE
        return Observable.create(new ObservableOnSubscribe<Integer>() {
            @Override
            public void subscribe(ObservableEmitter<Integer> e) throws Exception {
                for (int i = 0; i < 10; i++) {
                    e.onNext(i);
                }
                e.onComplete();
            }
        });
    }*/
}
