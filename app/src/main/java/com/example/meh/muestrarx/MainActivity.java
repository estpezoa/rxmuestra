package com.example.meh.muestrarx;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import java.util.concurrent.ThreadLocalRandom;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.functions.Cancellable;

public class MainActivity extends AppCompatActivity {

    Button btnGenerar;
    EditText etNumInicio, etNumFinal, etApuesta;
    TextView lblResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        btnGenerar = (Button) findViewById(R.id.btnGenerar);
        etNumInicio = (EditText) findViewById(R.id.etNumInicio);
        etNumFinal = (EditText) findViewById(R.id.etNumFinal);
        lblResultado = (TextView) findViewById(R.id.lblResultado);
        etApuesta = (EditText) findViewById(R.id.etApuesta);

        createButtonClickObservable()
                .filter(resultado -> (Integer.parseInt(resultado) == Integer.parseInt(etApuesta.getText().toString())))
                .subscribe(resultado -> lblResultado.setText("Bien"));



        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    //OBSERVABLE BOTON

    private Observable<String> createButtonClickObservable(){
        return Observable.create(new ObservableOnSubscribe<String>() {
            @Override
            public void subscribe(ObservableEmitter<String> e) throws Exception {
                btnGenerar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        lblResultado.setText("Mal");
                        int randomNum = ThreadLocalRandom.current()
                                .nextInt(Integer.parseInt(etNumInicio.getText().toString()),
                                        Integer.parseInt(etNumFinal.getText().toString())+1);

                        //ON NEXT TOMA UN PARAMETRO EMITIDO POR EL OBSERVABLE
                        e.onNext(String.valueOf(randomNum));
                    }
                });

                e.setCancellable(new Cancellable() {
                    @Override
                    public void cancel() throws Exception {

                    }
                });
            }
        });
    }
}
